# TODO list

This is a Kanban-style list of the features that shall be implemented in Brian2Lava. Necessary fixes may appear as subtasks. Note that we sometimes have to distinguish between features intended
for one of the two major functions of the software, "flexible-model mode" and "preset-model mode". The former enables to flexibly generate and run models defined in Brian format with a floating-point
CPU backend, while the latter enables running pre-defined models on Loihi 2 as well as floating- or fixed-point CPU backends.


### Backlog
- Multiple synaptic pathways [in preset-model mode]
  - [ ] implementation
  - [ ] testing
- Multiple synaptic pathways [in flexible-model mode]
  - [x] implementation
  - [ ] fix: support for delays for multiple synaptic variables (related issue: [#38](https://gitlab.com/brian2lava/brian2lava/-/issues/38))
  - [ ] testing
- Fix STDP example [in flexible-model mode] (related issue: [#39](https://gitlab.com/brian2lava/brian2lava/-/issues/39))
- Inter-process variable sharing for initialization: (e.g. `S.w = exp(-(x_pre-x_post)**2/(2*width**2))`, where x_pre and x_post are variables from a NeuronGroup) [in flexible-model mode]
  - [ ] enabling to use neuron variables to define connection conditions (related issue: [#33](https://gitlab.com/brian2lava/brian2lava/-/issues/33))
- Linked variables (see [here](https://github.com/brian-team/brian2/blob/98a30dde875d673325c4900ec28a2e2091440978/brian2/core/variables.py#L739))
- Defining multiple clocks (related issue: [#36](https://gitlab.com/brian2lava/brian2lava/-/issues/36))
  - [ ] in flexible-model mode
  - [ ] in preset-model mode
- Custom scheduling ("reordering the computing order")
  - [ ] in flexible-model mode
  - [ ] in preset-model mode
- Profiling [in preset-model mode]
- [SummedVariableUpdater](https://github.com/brian-team/brian2/blob/98a30dde875d673325c4900ec28a2e2091440978/brian2/synapses/synapses.py#L115) (when using the codeword `(summed)` in equations)
  - [ ] in preset-model mode, possibly with helper neuron workaround
  - [ ] in flexible-model mode
- SpatialNeuron (see [example](https://github.com/brian-team/brian2/blob/98a30dde875d673325c4900ec28a2e2091440978/brian2/spatialneuron/spatialneuron.py#L170))
- [PopulationRateMonitor](https://github.com/brian-team/brian2/blob/98a30dde875d673325c4900ec28a2e2091440978/brian2/monitors/ratemonitor.py#L18)
- Spike monitor `record = False` implementation [in flexible-model mode] (related issue: [#37](https://gitlab.com/brian2lava/brian2lava/-/issues/37))
- Nested subexpressions support in synapses (related issue: [#34](https://gitlab.com/brian2lava/brian2lava/-/issues/34))
- Script generator: Add to the generated files a self contained Lava script which mirrors the intended Brian simulation to allow debugging and further customization of a simulation without being bound by Brian2Lava.
- complying with flake8 recommendations
- PoissonGroup [in preset-model mode]: a workaround already exists (see [here](https://gitlab.com/brian2lava/brian2lava/-/blob/main/examples/exc_inh_network_brunel_2000.ipynb) or [here](https://gitlab.com/brian2lava/brian2lava/-/blob/main/dev/test_probspiker_loihi2.ipynb)), however, implementing `PoissonInput` will provide more benefits
- reorganizing tests internally: separate more clearly between flexible and preset mode


### To do next
- documentation infrastructure
  - [x] remove the compiled docs from the "brian2lava" repo
  - [ ] build and deploy docs automatically - either in "docs" repo, or in website repo (cf. `.gitlab-ci.yml` in "docs" repo)
  - [ ] provide not only the docs of the latest release, but also from other releases and the latest code version
- use `check_for_brian2lava_support` wherever possible
- consider Brian's integration step in Lava Processes (fix the problem with inconsistent results by changing the value of the weight matrix to account for the `dt`) [in preset-model mode]
  - [ ] fixed-pt. (in general, including F2F)
- [Subgroups](https://github.com/brian-team/brian2/blob/98a30dde875d673325c4900ec28a2e2091440978/brian2/groups/subgroup.py#L10)
- [PoissonInput](https://brian2.readthedocs.io/en/stable/reference/brian2.input.poissoninput.PoissonInput.html) - is expected to tremendously increase performance in examples like Brunel (2000); `numpy.random.binomial` may be used on CPU, and built-in random generation may be used on Loihi 2
- delays in synaptic transmission [in preset-model mode]
- add a mentioning of the relative error to the `check_convert_int` function
- further examples from Brian docs 
- further examples for new neuron models (e.g., for ProbSpiker and ATRLIF)
- learning rules [in preset-model mode] (related issue: [#28](https://gitlab.com/brian2lava/brian2lava/-/issues/28))
- improved display of equations objects, i.e., "V" -> "volt"; maybe Brian offers according opportunities (see, e.g. `user_equations`)
- random number utilization on Loihi
  - [ ] improve the algorithm
  - [ ] throw a warning when using `seed` in Loihi model (there is no seed to be set for temp state random numbers)
- use Lava's new Sparse process for synapses
- add NcProcessModel for `LIF_rp_v_input` to model library
- add NcProcessModel for `LIF_predef_stim_versatile` to model library
- support for [TimedArray](https://brian2.readthedocs.io/en/stable/reference/brian2.input.timedarray.TimedArray.html) [in preset-model mode]


### Doing


### Done (v1.0.0-beta.2)
- SpikeMonitor [in preset-model mode]
  - [x] support on Loihi 2 (but different problems persist, see related issue [#40](https://gitlab.com/brian2lava/brian2lava/-/issues/40))
  - [ ] indices selection (requires Subgroups implementation because Brian does not support `record` parameter for `SpikeMonitor`)
- new neuron models
  - [x] Adaptive-Threshold-and-Reset LIF (ATRLIF)
  - [ ] Adaptive-Voltage LIF (AVLIF)
  - [x] several LIF neuron variants
  - [x] ProbSpiker
  - [ ] TimeSpiker (implemented for the learning rules feature, not yet included in the release)
  - [ ] Izhikevich?
- Synapses without learning in preset-model mode
  - [x] fix: non-square synapse matrices cannot be created (related issue: [#43](https://gitlab.com/brian2lava/brian2lava/-/issues/43))
  - [x] fix: `i` and `j` are switched when connecting a `SpikeGenerator` (related to previous point)
  - [x] fix: currently only one `Synapses` object can be connected to a neuron group (`DuplicateConnectionError(...), 'Cannot connect same ports twice.'`related issue: [#45](https://gitlab.com/brian2lava/brian2lava/-/issues/45))
  - [x] throw a warning if an unsupported synapse model is used (related issue: [#50](https://gitlab.com/brian2lava/brian2lava/-/issues/50))
- include tests in releases
- improved example notebooks (including extensive comparisons across backend platforms)
- Brunel (2000) example notebook
  - [x] on CPU, similar to [this](https://brian2.readthedocs.io/en/stable/examples/frompapers.Brunel_2000.html) Brian 2 example
  - [x] on Loihi 2
  - [ ] fix weight readout error on Loihi 2 (related issue: [#48](https://gitlab.com/brian2lava/brian2lava/-/issues/48))
  - [ ] fix spike monitor error on Loihi 2 (related issue: [#40](https://gitlab.com/brian2lava/brian2lava/-/issues/40))
- StateMonitor [in preset-model mode]
  - [x] reduce code of `init_lava_monitors()` and `init_lava_state_probes()`
  - [x] fix: monitored data points are sometimes lost on Loihi (related issue: [#44](https://gitlab.com/brian2lava/brian2lava/-/issues/44))
- make LIF neuron models in preset mode more universal (e.g., add variables)
  - [x] reversal potential constant
  - [ ] membrane resistance constant
  - [x] refractory period
  - [x] improve use of `delta_*` and `tau_*` (currently model.json says `tau_*` are required -- also mention that a `delta_*` value will be calculated and is used for further computation)
- tests for preset mode/Loihi
  - [x] test synapses
  - [x] test monitors
  - [ ] testing LIF with F2F (later)
- besides the equations, check if the conditions (such as "th" and "rs") match with preset model as well
  - [x] implemented
  - [x] fix: support list of "rs" sub-conditions in condition checking
  - [x] select models which fits best
- restructure/simplify bit shift operations
  - [x] do conversion of incoming activation already in B2L (currently there is 6-bit shift in LIF process model)
  - [x] remove bit alignments from example notebooks (shall only be done in Brian2Lava core code and Lava models)
- encapsulate all print statements in logger
- fixed `lava_workspace` issue with relative paths
- individual weight exponents (array of exponents) supported
  - [x] in Brian2Lava
  - [x] in customized Dense process taken from Lava (within Brian2Lava)
  - [ ] in Dense process shipped with Lava (requires Lava pull request)
- Float2Fixed conversion [in preset-model mode]
  - [ ] fine-tuning to restricted bit-ranges to avoid overflows (still rough approximation)
  - [x] optimization of the supported range of parameter values
  - [x] fitting: either achieving `dt = 1`, and flaws in numerical correctness, *or* scaling properly
        -> treat nonlinearities in equations
        -> enabled using custom-tailored F2F conversion for each model individually (via `*_f2f.py` files, which are already available for many models)
  - [x] docs: example for base-2 instead of base-10
  - [ ] Jupyter notebook with generic/preset mode example for neurons and synapses with F2F conversion
  - [x] `set_binary_accuracy` and`set_decimal_accuracy` are not anymore needed to increase the precision increases anymore - but accuracy _decreases_ will be useful when overflows occur (we should notify users about this)
  - [x] examples in standard models library 
  - [ ] customization (`*_f2f.py` files) for all models
- added `__version__` to `brian2lava.__init__.py`
- TimedArray
  - [x] enable the use of TimedArray objects via special treatment of variable expressions
  - [ ] enable to define the parameters of the special expression in the model definition
- fix: occasional `ModuleNotFoundError` in simulations with multiple neuron groups (related issue: [#47](https://gitlab.com/brian2lava/brian2lava/-/issues/47))
- cleanup in preset-model handler (related issue: [#51](https://gitlab.com/brian2lava/brian2lava/-/issues/51))
- complete API (autodoc) documentation
  - [x] add all classes and parameters
  - [x] ensure correct RST formatting
  - [ ] complete description


### Done (v1.0.0-beta.1) 
Mainly working on preset-model mode for this release. 
- Synapses without learning in preset-model mode
  - [x] generating synapses with an expression
  - [x] fix: `No ProcessModels found with tag 'ucoded' for Process synapses::Dense.`
  - [x] fix: synapses affect processes/neuron groups they are not connected to (related issue: [#42](https://gitlab.com/brian2lava/brian2lava/-/issues/42))
  - [ ] fix: non-square synapse matrices cannot be created (related issue: [#43](https://gitlab.com/brian2lava/brian2lava/-/issues/43))
  - [ ] fix: `i` and `j` are switched when connecting a `SpikeGenerator` (possibly related to previous point)
  - [ ] fix: currently only one `Synapses` object can be connected to a neuron group (`DuplicateConnectionError(...), 'Cannot connect same ports twice.'`related issue: [#45](https://gitlab.com/brian2lava/brian2lava/-/issues/45))
- Preset-model mode (necessary to deal with restrictions on Loihi; CPU implementation for comparison; related issue: [#23](https://gitlab.com/brian2lava/brian2lava/-/issues/23))
  - [x] Loihi support
  - [x] choosing between Loihi, floating- and fixed-pt. CPU implementation
- Two modes for Brian2Lava
  - [x] Renamed `CPU` -> `flexible`, renamed `Loihi2` -> `preset`; adapted the `set_device` function with the new naming convention
  - [x] dynamical loading of external model library
  - [x] Git submodule to include a public version of the model library
  - [x] Introduced the preset-model mode (merged the `feature/preset_mode` branch into `main`)
- SpikeGenerator support [in preset-model mode]
  - [x] Support on CPU
  - [x] Support on Loihi 
- consider Brian's integration step in Lava Processes (fix the problem with inconsistent results by changing the value of the weight matrix to account for the `dt`) [in preset-model mode]
  - [x] floating-pt.
  - [x] fixed-pt. (for specific models)
  - [ ] fixed-pt. (in general, including F2F)
- StateMonitor [in preset-model mode]
  - [x] support on CPU
  - [x] support on Loihi 2 using state probes (Lava doesn't provide monitors for Loihi 2 yet `Exception: Connecting Nc OutPort to Py InPort is not supported.`; related issue: [#40](https://gitlab.com/brian2lava/brian2lava/-/issues/40))
  - [x] time tracking for state probe monitors analogous to usual monitors
  - [ ] reduce code of `init_lava_monitors()` and `init_lava_state_probes()`
  - [ ] fix (if possible): monitored data points are sometimes lost on Loihi (related issue: [#44](https://gitlab.com/brian2lava/brian2lava/-/issues/44))
- Documentation of the requirements to run simulations on Loihi
  - [x] Loihi Synapses documentation
  - [x] Information on hardware, mode, number representation, and other choices
  - [x] Simulation modes (flexible & preset)
  - [x] Float2Fixed (current state)
  - [x] Module loading/preset model library
- LIF neurons [in preset-model mode]
  - [x] on CPU floating-pt.
  - [x] on CPU fixed-pt.
  - [x] on Loihi
  - [x] enable the setting of firing threshold `v_th` and reset voltage `v_rs` (using the actual variable names from the equations of pre-defined models)
  - [ ] improve use of `delta_*` and `tau_*` (currently model.json says `tau_*` are required -- also mention that a `delta_*` value will be calculated and is used for further computation)
- Jupyter notebooks with examples
  - [x] flexible mode example with neurons and synaptic plasticity
  - [x] preset mode/generic example with neurons and synapses (with Loihi 2 support; no F2F conversion)
  - [ ] preset mode/generic example with neurons and synapses with F2F conversion
  - [ ] examples from Brian docs
- add a note to the docs and code indicating the `/tmp` issue with state probes in Lava (https://github.com/lava-nc/lava/issues/799)
- add 'conditions' field to model.json (e.g., to specify threshold and reset voltage expressions for LIF neuron)
- tests for preset mode/Loihi
  - [x] testing LIF (without F2F)
  - [ ] testing LIF (with F2F)
  - [ ] test synapses (needs SpikeGenerator)
  - [ ] test monitors (kinda needs SpikeGenerator)
- SpikeMonitor [in preset-model mode]
  - [x] support on CPU
  - [ ] support on Loihi 2 using `RingBuffer` (with Lava monitor: `Exception: Connecting Nc OutPort to Py InPort is not supported.`; related issue: [#40](https://gitlab.com/brian2lava/brian2lava/-/issues/40))
  - [ ] indices selection (to be tested)
- Float2Fixed conversion [in preset-model mode] 
  - [x] first implementation 
  - [x] `fixed_to_float` switch in `set_array_data` (enabling to disable the conversion when it shall not be used)
  - [ ] fine-tuning to restricted bit-ranges to avoid overflows (still rough approximation)
  - [x] change from decimal to binary base (could improve rounding)
  - [ ] optimization of the supported range of parameter values (in a later release)
  - [ ] fitting: either achieving dt = 1, and flaws in numerical correctness, *or* scaling properly
        -> treat nonlinearities in equations
  - [ ] docs: example for base-2 instead of base-10


### Done (previous releases)

- Learning rules [in flexible-model mode]
- Delays in synaptic transmission [in flexible-model mode]
- Initializing conditional variables with an expression [in flexible-model mode] (related issue: [#19](https://gitlab.com/tetzlab/brian2lava/-/issues/19))
- Array cache to read variable values before running the simulation [in flexible-model mode]
- Initialization queue (similar to [CPP main queue](https://github.com/brian-team/brian2/blob/master/brian2/devices/cpp_standalone/device.py#L191)) [in flexible-model mode]
- PoissonGroup [in flexible-model mode]
- SpikeGeneratorGroup [in flexible-model mode]
- Using neuron variables to define connection conditions (see [example](https://github.com/brian-team/brian2/blob/master/brian2/tests/test_synapses.py#L277)) [in preset-model mode]
- StateMonitor [in flexible-model mode]
- SpikeMonitor [in flexible-model mode]
- Testing enabled and tests added (in general; related issue: [#52](https://gitlab.com/brian2lava/brian2lava/-/issues/52))