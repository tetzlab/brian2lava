from brian2 import *
# Import Brian2Lava
import brian2lava

# Use included models directory
# models_dir=None

# Use external models directory (containing ncProcessModels for Loihi)
models_dir='/homes/francesco.negri/B2L/brian2lava-models/models'

# Set device, choosing fixed-point representation but switching off F2F conversion
set_device('lava', mode='preset', hardware='Loihi2', num_repr='fixed', use_f2f=False, models_dir=models_dir) # use preset-model mode with CPU (fixed-pt. representation)
BrianLogger.log_level_debug()

# Import LIF model
from brian2lava.preset_mode.model_loader import lif

# Set seed
seed(1)

# Set defaultclock
defaultclock.dt = 1 * second #0.5*ms

## Define conversion factor
cf = 1000

# Define parameters
N = 3
bias = 0.015 * cf * mV/ms
tau_j = 0 * cf * ms
tau_v = 10 * cf * ms
v_th = 100 * cf * mV
v_rs = -5 * cf * mV
ref = 2 * cf

# Add a spike generator group
S = SpikeGeneratorGroup(3, [0, 1], [2, 6]*second) # <--- only works with N neurons!

# Add a neuron group (which receives a background input)
P = NeuronGroup(N, lif.equations, threshold=lif.conditions["th"], reset=lif.conditions["rs"], refractory='ref*ms', method='euler')
P.j = "0.05 * cf * mV/ms"
P.v = "2 * cf * mV"

# Add another neuron group (which does not receive a background input)
Q = NeuronGroup(N, lif.equations, threshold=lif.conditions["th"], reset=lif.conditions["rs"], refractory='ref*ms', method='euler')
#Q.j = "0.1*mV/ms"
Q.v = "2 * cf * mV"

# Add synapses
syn = Synapses(S, Q, model = 'w : volt/second', on_pre='j_post += w')
syn.connect(i=[0], j=[0,1,2])
syn.w = 50 * cf * mV/second

# Add monitor for spikes
spmP = SpikeMonitor(P, variables='v', record=True)
spmQ = SpikeMonitor(Q, variables='v', record=True)

# Add monitors for voltage and current
stmPv = StateMonitor(P, variables='v', record=True)
stmPj = StateMonitor(P, variables='j', record=True)
stmQv = StateMonitor(Q, variables='v', record=True)
stmQj = StateMonitor(Q, variables='j', record=True)
spkmP = SpikeMonitor(P)

# Run simulation
#run(15*ms)
run(30 * second)

print(spmP.i)
print(spmP.t/second)
print(spmQ.i)
print(spmQ.t/second)
print(f"NeuronGroup P: {len(spmP.i)} spikes occurred in total.")
print(f"NeuronGroup Q: {len(spmQ.i)} spikes occurred in total.")