# v1.0.0-beta.2

This (overdue) release brings a number of new features and improvements for [preset mode](https://brian2lava.gitlab.io/docs/user_guide/modes.html) (note that this is currently the only mode undergoing development).

## New features

- `SpikeMonitor` support for Loihi 2 (but with restrictions, as pointed out below)
- new neuron models added to the standard library:
  - Adaptive-Threshold-and-Reset LIF (ATRLIF)
  - ProbSpiker
  - several LIF neuron variants (supporting the use of reversal potentials, refractory period, different input types)
- new and improved example notebooks (including extensive comparison across backend platforms) 
- preset model selection: checking if the conditions (e.g., for threshold crossing and reset) match and selecting the model which fits best; other improvements and fixes
-  support for individual exponents (array of exponents) in mantissa-and-exponent representation of weights (but requires custom Lava implementation)
- Float2Fixed conversion (in testing stage)
  - optimization of the supported range of parameter values
  - fitting for each model individually through custom-tailored F2F conversion (via `*_f2f.py` files, which are already provided for most models)
  - examples in standard models library

## Bug fixes and other changes

- several fixes for synapses
  - [x] fix: non-square synapse matrices cannot be created (related issue: [#43](https://gitlab.com/brian2lava/brian2lava/-/issues/43))
  - [x] fix: `i` and `j` are switched when connecting a `SpikeGenerator` (related to previous point)
  - [x] fix: currently only one `Synapses` object can be connected to a neuron group (`DuplicateConnectionError(...), 'Cannot connect same ports twice.'`related issue: [#45](https://gitlab.com/brian2lava/brian2lava/-/issues/45))
  - [x] throw a warning if an unsupported synapse model is used (related issue: [#50](https://gitlab.com/brian2lava/brian2lava/-/issues/50))
- fix for lost data points with `StateMonitor` (related issue: [#44](https://gitlab.com/brian2lava/brian2lava/-/issues/44))
- more tests for preset mode (in particular, targeting synapses and monitors)
- streamlined testing: enabled Loihi 2 tests by default, with automatic skipping when the Loihi 2 process directory is not accessible
- restructured/simplified bit shift operations (now mostly done within the Brian2Lava module, and not in example notebooks or Lava models)
- all output encapsulated in logger
- fixed state probe issues with temporary directory
- fixed `lava_workspace` issue with relative paths
- for `tau_*`, now mentioning that a `delta_*` value will be calculated and is used for further computation (further, direct definition of `delta_*` parameters in Brian script is now possible)
- clipping of decay constants and probabilities if maximum bit width is exceeded
- enhanced documentation, with integrated example notebooks, comprehensive API documentation, and other improvements
- fixed occasional `ModuleNotFoundError` in simulations with multiple neuron groups (related issue: [#47](https://gitlab.com/brian2lava/brian2lava/-/issues/47))
- added `__version__` to `brian2lava.__init__`

## Known issues

> **Note:** More information may be provided in the [documentation](https://brian2lava.gitlab.io/docs/) or in the linked issues.

**Bugs and unexpected behavior:**

The order of the list shall reflect the priority. Keep an eye on the [repo](https:/gitlab.com/brian2lava/brian2lava/) for possible fixes before the next release.
- `SpikeMonitor` support for Loihi 2 still has restrictions, partially due to Lava (see related issue [#40](https://gitlab.com/brian2lava/brian2lava/-/issues/40))
- the F2F converter is still in a testing stage
- wrong compute order in STDP example in flexible mode (related issue: [#39](https://gitlab.com/brian2lava/brian2lava/-/issues/39))

**Missing features:** 

The Brian 2 features in this list will be gradually added to Brian2Lava over the next releases. You can roughly interpret the order of this list as a timeline.
- setting `record = False` for spike monitors
- learning rules in preset mode (related issue: [#28](https://gitlab.com/brian2lava/brian2lava/-/issues/28))
- synaptic transmission delays in preset mode
- multiple synaptic pathways in preset mode
- delays for multiple synaptic pathways in flexible mode (related issue: [#38](https://gitlab.com/brian2lava/brian2lava/-/issues/38))
- `PoissonGroup` in preset mode (but a workaround is possible using `NeuronGroup`, cf. [here](https://gitlab.com/brian2lava/brian2lava/-/blob/main/examples/exc_inh_network_brunel_2000.ipynb) or [here](https://gitlab.com/brian2lava/brian2lava/-/blob/main/dev/test_probspiker_loihi2.ipynb))
- `PoissonInput`
- `SummedVariableUpdater` (when using the codeword `(summed)` in equations)
- `PopulationRateMonitor`
- using neuron variables to define connection conditions (related issue: [#33](https://gitlab.com/brian2lava/brian2lava/-/issues/33))
- `LinkedVariable`
- defining multiple clocks (related issue: [#36](https://gitlab.com/brian2lava/brian2lava/-/issues/36))
- `SpatialNeuron`
- Subgroups
- nested subexpressions in synapses (related issue: [#34](https://gitlab.com/brian2lava/brian2lava/-/issues/34))

## Full list of changes

See the [code comparison](https://gitlab.com/brian2lava/brian2lava/-/compare/v1.0.0-beta.1...v1.0.0-beta.2) in the Brian2Lava repository.


# Past releases:


# v1.0.0-beta.1

## New features

The software now features two modes, `flexible` and `preset`. The former enables to flexibly generate and run models defined in Brian format with a floating-point CPU backend. The latter enables running pre-defined models on Loihi 2 as well as floating- or fixed-point CPU backends. Note that previous versions of Brian2Lava (up to v1.0.0-alpha.3) only featured `flexible` mode. Thus, the vast majority of changes in the present release are features related to the newly implemented `preset` mode:
- easily choosing between Loihi 2, floating-point and fixed-point CPU implementation via the the `set_device` function, which allows to specify the used backend including the mode, hardware, and number representation
- LIF neurons for all backends in preset mode (including adjustable firing threshold `v_th` and reset voltage `v_rs`)
- `SpikeMonitor` support for preset mode on CPU
- `StateMonitor` support for preset mode on CPU and Loihi 2
- support for synapses (without plasticity) in preset mode
- dynamical loading of external model library for preset mode
- included a public version of the model library in the Brian2Lava release via a Git submodule
- `SpikeGenerator` support for preset mode on CPU and Loihi 2
- documentation of all requirements to run simulations in preset mode/on Loihi 2
- Jupyter notebooks with examples for
  * flexible mode with neurons and synaptic plasticity
  * preset mode with neurons and synapses (including Loihi 2 backend)

## Bug fixes and other changes

- first development version of the Float2Fixed converter for preset mode 
- first tests for neurons and synapses in preset mode (including Loihi 2 backend)

## Known issues
> **Note:** More information may be provided in the [documentation](https://brian2lava.gitlab.io/docs/) or in the linked issues.

**Bugs and unexpected behavior:**

The first items in this list will be targeted with highest priority in the next release process, so keep an eye on the [repo](https:/gitlab.com/brian2lava/brian2lava/) over the next weeks. 
- `Synapses` objects with non-square connection matrices cannot be created in preset mode (related issue: [#43](https://gitlab.com/brian2lava/brian2lava/-/issues/43))
- indices `i` and `j` are switched when connecting a `SpikeGenerator` (possibly related to the previous point)
- there is no specific mentioning that a `delta_*` value is calculated and is passed to preset models for further computation (instead of the models using `tau_*` directly)
- the F2F converter is still under development, it should only be used with care
- wrong compute order in STDP example in flexible mode (related issue: [#39](https://gitlab.com/brian2lava/brian2lava/-/issues/39))
- monitored data points are sometimes lost on Loihi 2 (Lava issue; see: [#44](https://gitlab.com/brian2lava/brian2lava/-/issues/44))
- state probes on Loihi 2 hardware may be not working (Lava issue) - please check if [this](https://github.com/lava-nc/lava/issues/799) has already been fixed in your Lava version

**Missing features:** 

The features in this list will be gradually added to Brian2Lava in the next releases. You can roughly interpret the order of this list as a timeline.
- `SpikeMonitor` on Loihi 2 hardware (related issue: [#40](https://gitlab.com/brian2lava/brian2lava/-/issues/40))
- setting `record = False` for spike monitors
- learning rules in preset mode (related issue: [#28](https://gitlab.com/brian2lava/brian2lava/-/issues/28))
- synaptic transmission delays in preset mode
- `PoissonGroup` in preset mode
- currently only one `Synapses` object can be connected to a neuron group in preset mode (related issue: [#45](https://gitlab.com/brian2lava/brian2lava/-/issues/45))
- multiple pathways in preset mode
- delays for multiple synaptic variables in flexible mode (related issue: [#38](https://gitlab.com/brian2lava/brian2lava/-/issues/38))
- `PoissonInput`
- `SummedVariableUpdater` (when using the codeword `(summed)` in equations)
- `PopulationRateMonitor`
- using neuron variables to define connection conditions (related issue: [#33](https://gitlab.com/brian2lava/brian2lava/-/issues/33))
- `LinkedVariable`
- defining multiple clocks (related issue: [#36](https://gitlab.com/brian2lava/brian2lava/-/issues/36))
- `SpatialNeuron`
- Subgroups
- nested subexpressions in synapses (related issue: [#34](https://gitlab.com/brian2lava/brian2lava/-/issues/34))


## Full list of changes

See the [code comparison](https://gitlab.com/brian2lava/brian2lava/-/compare/v1.0.0-alpha.3...v1.0.0-beta.1) in the Brian2Lava repository.


# v1.0.0-alpha.3

> **Note:** This release only included what is now called `flexible` mode. Meanwhile, `preset` mode with support for Loihi 2 has been introduced, and the overall structure of Brian2Lava has therefore changed substantially. The following Release Notes have been copied from [this wiki page](https://gitlab.com/brian2lava/brian2lava/-/wikis/Release-Notes/v1.0.0-Alpha-3).

Since this is the first of these Release Notes, we will make a small rundown of all of the currently supported features. On future releases, we will then start describing the updates more in detail.

## Loihi 2 support

First of all, let's address the elephant in the room: **_Loihi2 hardware is still not supported_.** This is due to a few factors:

* The need for an entirely different pipeline for code generation. At the moment, Loihi support in Lava is quite limited, so we have to adapt our pipeline significantly in order to be able to circumvent these limitations. Hopefully, in the future it will be possible to slowly fade back to the more general implementation used for the CPU backend.
* Currently, direct development of Loihi microcode is not open to public contributions, meaning that we are not allowed to share parts of our code due to their confidentiality.
* Until now, we were focusing on creating a solid foundation on the CPU backend so that as Lava development progresses we will be able to slowly move back to this more generalized approach bit by bit. As of now, the main structure of the device should be in place and the pipeline should work as intended. I say should because as the name of the release implies, we still need to perform some thorough testing.

With that being said, we are currently working on Loihi support, and you should expect some news regarding that over the next few updates. Unfortunately, we cannot promise that the full Loihi features will be available to the public, but if you are part of the INRC and have access to the source code for Lava, you should get in touch with us once we release the first Loihi-ready version!

With that out of the way, below we'll try to make a comprehensive list (which will probably need to be updated in case we forget something) of the currently supported (and not supported) features in the CPU implementation. We will separate the features thematically in order to hopefully give it a certain degree of order.

## Brian objects:

These are objects that can be directly instantiated by the user. Being in the Supported section means that they have been implemented, but some of their features might still be missing. In general we try to make sure that an object can cover its most important features before tagging it as supported. If during your simulations you encounter significant bugs/limitations, feel free to get in touch with us or open an issue!

**Supported:**

* `NeuronGroup`
* `Synapses`
* `PoissonGroup`
* `SpikeGeneratorGroup`
* `StateMonitor`
* `SpikeMonitor`

**Not Supported:**

* `PoissonInput`
* `PopulationRateMonitor`
* `SpatialNeuron`
* `SummedVariableUpdater` (instantiated through the use of the `(summed)` specifier in an equation)
* `LinkedVar` (instantiated by calling `linked_var` )

## Equations and Variable definitions:

These refer to all `string` related features, such as equation definitions, conditions, and variable definitions. Overall, these should be mostly working as expected (with one small exception) but we need more testing, so feel free to test out your 15 ODE model and let us know if it breaks.

**Equations**

* All equation types and specifiers (indicated with parentheses, such as `(unless refractory)`) are supported. One exception worth noting is the `(event driven)` specifier on synaptic code. Let's make a point dedicated to that (see the Additional Notes section).

**Variables**

* All variable initialization methods (expressions, indices and combinations of these) should be supported. This should come with an

  \`\*\` because there are still some minor issues which we will try to solve in a quick update.
* One known exception (which also applies to Synaptic connections below) are those expressions requiring access to the variable of another object. For example \`S.w = 'exp(-(x_pre-x_post)\*\*2/(2\*width\*\*2))'\`. where `x_pre` and `x_post` belong to a `NeuronGroup` .

**Other string expressions**

* All conditions for `reset,` `threshold` and Synapses `on_pre` and `on_post` expressions should be supported.
* All connection conditions for `Synapses` should be supported, including probabilities given through the `p` argument.

## Clocks

* Currently, multiple clocks are not supported. 
* One known issue with the current release is that defining a custom clock through the definition of a timestep `dt` is not working as expected. A workaround for this problem can be found in #36 , so even with this limitation, a custom timestep can be used for your simulations. You can expect this to be fixed in the next release.

## Additional Notes

* **Optimization:** Even though the CPU implementation is based on numpy code generation, there are some important limitations in terms of optimization of the compute resources. One clear example lies in monitoring select indices from a neurongroup or synapse variable. Lava `Monitor`s do not support this feature as of now, meaning that in our implementation we will record all indices, and only after the simulation will select the indices specified by the user and ditch the remaining data.
* **The `(event driven)` specifier:** Due to the asynchronicity built into Lava, event driven computation is discouraged. For learning related code, this is solved by introducing separate phases during the same timestep dedicated to different types of computation. The learning phase, for example, is only started once all of the spikes being sent around in the network have been sent and collected by the receivers. This prevents running the same code multiple times resulting in strange behavior and possible deadlocks. For this same reason, using the `(event driven)` specifier to run synaptic code code only if a spike is received during this timestep could lead to unwanted behavior. At the moment, this feature is not behaving in the same way as in Brian2, so while using it will currently only show a WARNING, in future updates we will probably raise an Exception. This problem is a bit more nuanced, though, and for this reason we might come back to it in the future.
* **Feature requests**: From now on we will focus more heavily on Loihi support and only concentrate on bugfixing for the CPU implementation. If any of the above mentioned limitations -or any missing feature you come across which is not listed here- is particularly annoying to you, please let us know. We will base our future updates on user feedback, so if you request some feature it is likely that it will be implemented in a future update. Or if you want to help us out, you can contribute directly to the project by opening a pull request!
* **Missing features list**: For a more comprehensive list of the known missing features, see the corresponding wiki page: [Missing Features](missing-features). We try to keep this page up to date as we find new missing features, according to your feedback and open issues.
