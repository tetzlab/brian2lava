=======================
Important notes on Lava
=======================

The `Lava neuromorphic computing framework <https://lava-nc.org/>`_ already provides a large set of robust functions to manage and run code on CPU and neuromorphic hardware. 
To this end, it uses so-called *Processes*, implemented by *Process Models* for different backend systems.

It has to be noted that Lava is still under development.

Furthermore, the following points are important to note:

* implementing *Processes* on Intel's Loihi 2 hardware requires writing specific microcode (which is not open to the public yet),
* monitors in Lava behave differently compared to monitors in Brian (also see :doc:`here </user_guide/monitors>`).

