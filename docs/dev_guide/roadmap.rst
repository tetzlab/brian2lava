=======
Roadmap
=======

For an overview of missing features, bugs, etc., please see the 
`Issues section <https://gitlab.com/brian2lava/brian2lava/-/issues/>`_ or the
`TODO list <https://gitlab.com/brian2lava/brian2lava/-/blob/main/dev/TODO.md>`_ 
in our GitLab repository.
