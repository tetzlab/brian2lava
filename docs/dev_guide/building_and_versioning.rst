=======================
Building and versioning
=======================

Building / release procedure
============================

The following information describes the steps to follow for creating a Brian2Lava release.

**Release steps checklist**

- Run linting locally (see :ref:`Linting <linting>`)
- Run tests locally (see :ref:`Testing <testing>`)
- Update ``README.md``
- Update ``docs/conf.py`` (especially, version number; also see :ref:`Versioning <versioning>`)
- Build documentation (see :ref:`Build docs <build_docs>`)
- Update ``.gitlab-ci.yml`` if necessary
- Update ``__init__.py`` (version number)
- Update ``setup.py`` (especially, version number)
- Update ``dev/TODO.md``, and accordingly, release notes in ``RELEASES.md`` (see :ref:`Preparing release notes <prepare_release_notes>`)
- Build package (see ``build.sh`` script)
- Install package and run tests (see :ref:`Test installation of pip package <pip_test_installation>`)
- Package upload test run (to test.pypi.org, see ``build.sh`` script)
- Install package from ``testpip`` and run an example (see :ref:`Test installation of pip package <pip_test_installation>`)
- Package upload (to pypi.org, see ``build.sh`` script)
- Add tag on GitLab
- Create release on GitLab (based on Tag) with brief release message
- Upload documentation (see :ref:`Upload docs <upload_docs>`)
- Update website

.. _linting:

**Linting**

Run

:: 

    python3 -m flake8 . --count --select=E9,F63,F7,F82 --ignore=F821 --show-source --statistics

Note that ``--ignore=F821`` is used to suppress irrelevant warnings from Lava process models.


.. _testing:

**Testing**

Run

:: 

    python3 -m pytest

Furthermore, manually test all examples and dev notebooks, if possible.

.. _prepare_release_notes:

**Preparing release notes**

- In ``TODO.md``, move all points from "Doing" to "Done (vXYZ)" or "To do next", depending on their state
- Go through the commits - possibly there were new features and bugfixes that are not yet mentioned in ``TODO.md``
- Based on the "Done (vXYZ)" section in ``TODO.md``, write the release notes with sections "New features", "Bug fixes and other changes", "Known issues", "Bugs and unexpected behavior", "Missing features"
  - Decide for each case if a separate mentioning is needed
  - Add unchecked subpoints to the "Bugs" or "Missing features" section, if this is sensible

**Build and upload package**

The bash script ``build.sh`` helps to lead through the packaging and uploading process.


.. _pip_test_installation:

**Test installation of pip package**

Create a virtual environment and activate it:

:: 
    
    python3 -m venv testenv
    source testenv/bin/activate

Upgrade pip:

:: 
    
    python3 -m pip install --upgrade pip


*ALTERNATIVE 1*: Install Brian2Lava including its dependencies from local wheel (replace ``xyz`` by the actual description of the latest wheel):

:: 
    
    python3 -m pip install --upgrade dist/brian2lava-xyz.whl


*ALTERNATIVE 2*: Install Brian2Lava including its dependencies from 'test' pip repository:

:: 
    
    python3 -m pip install --upgrade brian2lava --extra-index-url=https://test.pypi.org/simple/


*ALTERNATIVE 3*: Install Brian2Lava including its dependencies from standard pip repository:

:: 
    
    python3 -m pip install --upgrade brian2lava


Run the tests via:

:: 
    
    python3 -m brian2lava.tests.run_tests


Possibly also test with the Jupyter notebooks from the ``examples`` and/or ``dev`` folder of the source code:

:: 
    
    git clone https://gitlab.com/brian2lava/brian2lava.git
    cd brian2lava
    jupyter lab

When finished, don't forget to deactivate the virtual environment:

:: 

    deactivate


Build and upload documentation (Sphinx handling)
================================================

Until we introduce versioning for the documentation, this updating procedure shall only be done when a new release is created.

**Init (usually not required)**

The following steps are needed to initialize the docs (only necessary when they shall be re-created completely; we keep it here for documentation purposes).

Run:

:: 

    cd docs
    sphinx-quickstart

Then, in ``docs/conf.py``, add

:: 
    
    import sys, os
    sys.path.insert(0, os.path.abspath(os.path.join('..', 'brian2lava', 'codegen')))
    sys.path.insert(0, os.path.abspath(os.path.join('..', 'brian2lava', 'device')))
    sys.path.insert(0, os.path.abspath(os.path.join('..', 'brian2lava', 'preset_mode')))
    sys.path.insert(0, os.path.abspath(os.path.join('..', 'brian2lava', 'utils')))
    sys.path.insert(0, os.path.abspath(os.path.join('..', 'brian2lava', 'writer')))
    sys.path.insert(0, os.path.abspath(os.path.join('..', 'brian2lava')))
    sys.path.insert(0, os.path.abspath('..'))

Further, change the HTML theme to ``'sphinx_rtd_theme'`` and add the extensions ``['sphinx.ext.napoleon', 'sphinx.ext.autodoc', 'nbsphinx']``.

In ``docs/index.rst``, add ``modindex`` wherever the module documentation shall be.

Finally, build the documentation. Assuming that we are in the ``docs`` directory:

:: 
    
    sphinx-apidoc -o . ../brian2lava
    sphinx-apidoc -o . ../brian2lava/codegen

For re-compiling use ``--force``, which causes existing files to be overwritten.

.. _build_docs:

**Build docs**

We first build the docs via ``make clean`` and then build the HTML files via ``make html``. We may combine these tasks in a single command in the following way:

:: 
    
    cd docs
    make clean html

.. _upload_docs:

**Upload docs**

Clone the docs repository:

:: 
    
    git clone git@gitlab.com:brian2lava/docs.git

Copy the built doc files from the ``brian2lava`` repository (within ``docs/_build/html``) to the ``docs`` repository and commit/push changes for the ``docs`` repository. 
After pushing the code, GitLab publishes the docs automatically `here <https://brian2lava.gitlab.io/docs/>`_.

.. _versioning:

Versioning
==========

For the first stage of development (testing stage with CPU support only), we were using the version nomenclature :code:`v1.0.0-alpha.N`, with :code:`N` the number of the alpha version.

For the current stage of development (testing stage including support for Loihi 2 hardware), we are using the version nomenclature :code:`v1.0.0-beta.N`, where :code:`N` is the number of the current beta version.

Eventually, we will proceed with :code:`v1.1.0`.
