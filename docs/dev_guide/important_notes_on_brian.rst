========================
Important notes on Brian
========================

.. add overview over Brian classes here

For multiple runs of the same script, ``device.reinit()`` followed by ``device.activate()`` needs to be called (also see :doc:`Importing and device setting </user_guide/import_set_device>`).