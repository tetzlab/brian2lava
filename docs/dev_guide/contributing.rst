============
Contributing
============

.. _contributor_roles:

Contributor roles
=================

Anyone who would like to contribute to Brian2Lava can open a :ref:`merge/pull request <pull_requests>` in our GitLab repository and thus become a **contributor**. Any contribution is highly appreciated and will be acknowledged! 
Before filing a merge/pull request, please read the guidelines below.

The repository is maintained by the Brian2Lava core developers (see `here <https://brian2lava.gitlab.io/#team>`__).


.. _pull_requests:

Merge/pull requests
===================
Merge/pull requests can be filed `here <https://gitlab.com/brian2lava/brian2lava/-/merge_requests>`__.
Please note that the version of Brian2Lava that the code to be merged is based on should be as new as possible. 
Thus, before filing the request, it's best to do a merge or rebase of the main branch in the branch to be merged. 

.. _code_style:

Code style
==========
For the code documentation, we use the numpydoc docstring format, which is described `here <https://numpydoc.readthedocs.io/en/latest/format.html>`__.

..
	flake8? black?

.. _branches:

Branches
========

Any major feature shall first be developed in a separate branch (ideally named ``feature/*``, ``bug/*``, etc.) before being merged into :code:`main`. The same goes for bugfixes or test development. 
Small fixes should preferably be pushed to the main branch first (otherwise be cherry-picked from other branches). 
The :code:`dev` branch should not be used anymore.


..
	.. _further_guidelines:
..
	Further guidelines
	==================
	<to be added>
