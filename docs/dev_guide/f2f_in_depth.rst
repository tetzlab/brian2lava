=======================
F2F conversion in-depth
=======================

*This page assumes you already have a general understanding on how we treat the F2F conversion. If you don't, 
we suggest to first read the more high-level explanation* :doc:`here </user_guide/float2fixed>`.

The current implementation of the F2F converter requires a bit of math to figure out the correct mapping
between floating point values and Lava-compatible fixed point parameters. This mapping is required due to the 
particular integration method used by Lava, which makes some assumptions about the model ODEs which result in 
a non trivial scalability in the parameter values. Below we'll show in depth why this is and how to find 
the correct mapping for each parameter in a Neuron model which will hopefully generalize to most use cases.

.. note:: 
    This method has a limitation: non-linear models are unfortunately not supported.
    For this feature a more sophisticated mapping should be defined, or alternatively, models on the lava-side
    (the ones defined in the preset model library) should be defined in a more brian-friendly manner (probably
    most sustainable but also more time-consuming).

.. note ::
    The F2F converter is currently still in a testing stage. Please let us know if you encounter any issues.


Differences between Brian and Lava ODE integration
==================================================
Let's use a simple example which will help illustrate the problems incurred when trying to scale floating point parameters
into Lava-compatible fixed point format. We refer to the outcome as "Lava-compatible" because strictly speaking lava parameters
are not really fixed point (for the most part), but simply integers. Nonetheless, in the following we'll just refer to them as fixed point.

Take the basic LIF model present in the Lava library. This model can be defined as:

.. math:: 
    \frac{dv}{dt} & = - \frac{v}{\tau_v} + u + b\\\\
    \frac{du}{dt} & = - \frac{u}{\tau_u} + a_{in}\ ,

where :math:`a_{in}` is just a binary function equal to the weight :math:`w` if a spike was received, 0 otherwise.

In fact, this is the equation that brian2lava uses as the preset equation for the ``lif`` model in the model library.
Note that this is different to many equations used for the LIF neuron, in which :math:`\tau_v` and :math:`\tau_u` 
multiply the derivatives on the left side of the equations. Part of the problem with the F2F convert arises specifically
because of this. You'll see why in a second. For now let's write out what an integration step for these equations would look
like in Brian 2 and in Lava.

**Brian 2**

In Brian, the integration step would translate to:

.. math:: 
    u(t+\mathrm{dt}) & = u(t)\left(1-\frac{\mathrm{dt}}{\tau_u}\right) + a_{in} \\\\
    v(t+\mathrm{dt}) & = v(t)\left(1-\frac{\mathrm{dt}}{\tau_v}\right) + u(t+1)\mathrm{dt} + b\mathrm{dt}\ .
    

Note that the activation is just added to the current *in block*, as its contribution is instantaneous
due to the delta-like spiking behavior of the model (assumed to apply also to incoming sources of activation).


**Lava**

In lava the integration is carried out as:

.. math:: 
    u(t+1) & = u(t)(1-du) + a_{in} \\\\
    v(t+1) & = v(t)(1-dv) + u(t+1) + b\ .

A few things are immediately apparent:

1. The absence of the ``dt``: This can be accounted for by assuming that dt':math:`= 1 = \alpha_t` dt. The second equality is inconsequential for now but 
   will come in handy later on.
2. The ``du`` and ``dv`` differentials. These can be interpred as corresponding to :math:`\mathrm{dt}/\tau`.
   In order to get closer to the Brian 2 representation we can then rewrite:

.. math:: 
    u(t+\mathrm{dt}') & = u(t)\left(1-\frac{\mathrm{dt'}}{\tau_u}\right) + a_{in} \\\\
    v(t+\mathrm{dt}') & = v(t)\left(1-\frac{\mathrm{dt'}}{\tau_v}\right) + u(t+\mathrm{dt})\mathrm{dt'} + b\mathrm{dt'}\ .

So that we are back to the same representation used by Brian. 

Converting to Fixed point
=========================

Now, let's define the conversion a bit more in detail:

We have a set of parameters :math:`S \in \mathbb{R}` to be converted to a second set :math:`S' \in \mathbb{N}`. 
We could approach this in two ways:
1. An exact method like the one used in Deep Learning for quantization of network parameters `[1] <https://www.tensorflow.org/lite/performance/quantization_spec>`_ .
this would require defining additional parameters and would thus result in an increase in the complexity of our models. This option may be explored in the future but 
is out of the scope of our project for the time being.
2. An approximate method consisting in scaling up and rounding all parameters of the simulation using some constants `P \in \R`, and trying to minimize the rounding
error by maximizing the value of our multiplicative constants. This is the approach used by our F2F converter

As a quick example, consider the conversion of: 

.. math::
    \pi = 3.14159.. 
    
to an integer representation. In order to minimize the rounding loss we want to multiply its value by the largest possible constant we can afford. 

In the realm of computation, the value of this constant will depend on a few factors, such as the amount of 
memory available to store a number (e.g. 8-bits,16-bits). In Lava-Loihi, each parameter is allocated a specific amount of memory, so we have to ensure that none of 
our parameter values exceeds their available memory. 

So let's say the maximum memory we can allocate for :math:`\pi` is 16 bits. Then the value of our multiplicative constant would be: 

.. math:: 
    A_{\pi}= (2^{16}-1)/\pi. 
    
So that in our simulation :math:`\pi` will correspond to :math:`2^{16}`. Of course, if we had another parameter
:math:`\beta = 2*\pi`, then :math:`\beta` would be converted to :math:`2^{16}`, and :math:`\pi` would become :math:`2^{15}`. 
This demonstrates how the largest parameter determines the value of the multiplicative constant used for the floating point to fixed point conversion.
Now, let's get back to the example of the LIF neuron to figure out a way to proceed with the conversion.

Instead of just blindly scaling each parameter by a fixed amount, it's useful to keep in mind that after a simulation is run, we will receive a series of fixed-point values corresponding to 
:math:`v_\mathrm{FP}(t)` and :math:`u_\mathrm{FP}(t)`, and in order to interface back with Brian and generate reasonable results, we will have to convert these parameters back into floating point numbers.
So in order to keep this process simple, we would like to find two parameters :math:`A` and :math:`B` so that:

.. math:: 
    v(t) & = Av_\mathrm{FP}(t) \\\\
    u(t) & = Bu_\mathrm{FP}(t)\ .

We can achieve this result by imposing this requirement on the differential timestep for the voltage variable, :math:`dv`, by multiplying left and right by :math:`A`:

.. math:: 
    Adv = -A\frac{v}{\tau_v}\mathrm{dt} + Au(t+\mathrm{dt})\mathrm{dt} + Ab\mathrm{dt}\ .

Now let's consider adding another constant :math:`\alpha_t`, and insert it into this equation by multiplyig by :math:`\alpha_t/\alpha_t`. Note that this effectively equates to multiplying
by 1, which has no effect on the equation. This yields:

.. math:: 
    Adv = -A\frac{v}{\alpha_t\tau_v}\alpha_t\mathrm{dt} + \frac{A}{\alpha_t}u(t+\mathrm{dt})\alpha_t\mathrm{dt} + \frac{A}{\alpha_t}b\alpha_t \mathrm{dt}\ .

We are now able to incorporate both of the constants :math:`A` and :math:`\alpha_t` into our variables and parameters by following the table:

-  :math:`\tau_i \rightarrow \tau_i' = \alpha_t \tau_i` (valid for both timescale parameters)
-  :math:`\mathrm{dt} \rightarrow \mathrm{dt}' = \alpha_t \mathrm{dt}`
-  :math:`b \rightarrow b' = A/\alpha_t b`
-  :math:`u \rightarrow u' = A/\alpha_t b` 
-  *(implied:)*  :math:`a_in \rightarrow a_in' = Ba_in`
-  :math:`v \rightarrow v' = A v`

This results in:

.. math:: 
    du' &= - \frac{u'}{\tau_u'}\mathrm{dt}' + a_in'\mathrm{dt}' = \frac{A}{\alpha_t}du\\\\
    dv' &= -\frac{v'}{\tau_v'}\mathrm{dt}' + u'\mathrm{dt}' + b'\mathrm{dt}'\ = Adv ,

Which shows that the resulting differential timesteps are equivalent to a scaled version of the original timesteps :math:`du` and :math:`dv`, exactly what we wanted to achieve.

Note how this also yields the relationship between the :math:`A` and :math:`B` parameters: :math:`B = A/\alpha_t`. Additionally, we can exploit another relationship that we highlighted
before to find the value of the :math:`\alpha_t` constant: since we know that the integration method in Lava implies that :math:`\mathrm{dt}' = 1`, then we can conveniently set:
:math:`\alpha_t = 1/\mathrm{dt}`, where dt is the timestep used by the Brian2 simulator. This effectively results in needing to only find the value of **one** scaling parameter: :math:`A`.

Secondly, note how all of the parameters in the model are processed by this scaling, satisfying the requirement that all of the parameters in our model are within integer range. 
It may seem obvious, but it's important to clarify that no additional approximation (apart from the imprecisions due to a finite-size integration step, coming from the Brian 2 simulator) 
of the parameter values is operated in this process. The loss in precision is entirely caused by the hardware limitations imposed by Loihi. In this sense, the outcome of this process does 
not affect the precision of the integration process. It also does not improve the precision of the integration method: it simply corresponds to a magnification of the integrated functions 
on both the time and variable axes, so that after the inverse conversion, we will be left with an exact point-by-point copy of the values that would be expected from running the simulation 
in vanilla Brian 2.

.. note:: 
    In the practical sense, the time variables :math:`\mathrm{dt}` and :math:`\tau` are not converted using this method. For the timestep this is not needed since Lava assumes 
    that it is 1, so the conversion is already implied. For the :math:`\tau` variables lava uses a different approach, in which the fractional decay is calculated in base 
    :math:`2^{12}` (to be scaled back after the calculation). Thus, for our purposes it is sufficient to calculate the fraction :math:`\mathrm{dt}/\tau` and multiply it by :math:`2^{12}`.
    The reason for this approach is that division is not supported on Loihi, so this fixed-point style computation is required to calculate the decay constants.

.. note:: 
    A similar discussion applies for probabilities related to random number generation. Since the RNG generates integers in the range :math:`[0, 2^{24})`, threshold probabilities will be 
    scaled to this range, too (e.g. :math:`p_\mathrm{spike} = 0.5 \rightarrow 2^{23}`).

Required files and code
=======================
The F2F converter is developed in a fully isolated manner, so that the execution of Brian2lava is unaffected when the converter is turned off. This results in the need
to compensate by adding some conversion specific code that will be accessed by the converter in order to perform the conversion. So, when introducing a new model to the preset library,
or as a custom user-model, a file should be added to the model's files if F2F conversion should be supported by brian2lava.
This file consists in a class definition, which will define the parameter transformations needed by this specific model. There are some requirements for this definition:

- **File name:** the f2f file should be named with the convention ``*_f2f.py`` (anything can replace the * character, the important factor is the ending of the file name).


- **Class requirements:**  the ``ModelScaler`` class should contain these fundamental attributes and methods:

    - ``forward_ops``, a dictionary of lambda functions to be applied to the parameters of the model for the conversion. You'll see by looking at the ``LIF_f2f.py`` code below that
      these functions correspond to the transformations defined in the table of the previous section.
    - ``optimal_scaling_params``, the only method which is actually called by the converter, should return a dictionary containing the calculated optimal scalings (:math:`A` and :math:`\alpha_t`)
    - ``process_class``: the class name for the Lava process corresponding to this model. 

An example, for the LIF neuron, is shown below. The additional methods and code only serve as helper code to identify the optimal scalings.

The ``LIF_f2f.py`` file 
-----------------------
Below is an example of ``f2f.py`` file which showcases the functionality required for the F2F to work on a LIF model.

.. code-block:: python
    :linenos:

    from brian2lava.utils.const import LOIHI2_SPECS
    class ModelScaler:
        process_class = 'LIF'
        forward_ops = {
            'v' : lambda alpha_t,A: A,
            'v_rs': lambda alpha_t,A: A,
            'v_th': lambda alpha_t,A: A,
            'dt': lambda alpha_t,A: alpha_t,
            'j': lambda alpha_t,A: A/alpha_t,
            'w': lambda alpha_t,A: A/alpha_t,
            'bias': lambda alpha_t,A: A/alpha_t,
        }
        # It's useful to differentiate variables and constants
        # since they are treated differently by Loihi
        variables = {'v','j'}
        # Variables to be MSB-aligned are defined in 'model.json'. To avoid copy-paste mistakes we 
        # define this variable at runtime using the instance from the F2F converter.
        msb_align_act = None
        msb_align_decay = None
        msb_align_prob = None
        const = None
        mant_exp = {'bias', 'w'}
        
        @staticmethod
        def max_val(var_name):
            if var_name in ModelScaler.variables:
                return LOIHI2_SPECS.Max_Variables
            elif var_name in ModelScaler.const:
                return LOIHI2_SPECS.Max_Constants
            else:
                return LOIHI2_SPECS.Max_Weights

        @staticmethod
        def min_scaling_params(variables):
            """
            Get the minimum scaling parameters to shift all of the parameters into 
            integer range. This is model specific.
            """
            # take the min of each variable
            dt,v,j,b = variables['dt'][0],variables['v'][0],variables['j'][0],variables['bias'][0]
            # If this neuron doesn't have any synapses connected to it, w won't be defined.
            w = variables['w'][0] if 'w' in variables else 0
            min_alpha_t = 1/dt
            # Avoid ZeroDivisionError
            params_to_max = []
            if v != 0:
                params_to_max.append(1/v)
            if j != 0:
                params_to_max.append(min_alpha_t/j)
            if w != 0:
                params_to_max.append(min_alpha_t/w)
            if b != 0:
                params_to_max.append(min_alpha_t/b)

            min_A = max(params_to_max)
            return {'alpha_t': min_alpha_t, 'A': min_A}
        
        @staticmethod
        def optimal_scaling_params(variables):
            """
            Since LIF neurons are static the optimal choice for the parameters
            corresponds to the maximal range of values allowed (so increasing 
            the scaling parameters to the largest possible)
            """
            return ModelScaler.max_scaling_params(variables)
        
        @staticmethod
        def max_scaling_params(variables):
            """
            The scaling of each variable shouldn't surpass the largest values
            allowed on Loihi2. This is not foolproof, but should be a good choice.
            In most cases we expect vth to be the one that defines the value of A.
            (The other parameters would have to be at least factor of 1/dt larger than vth)
            """
            from numpy import infty
            alpha_t = 1/variables['dt'][0]
            overall_max_A, max_A = infty, infty
            for var_name, (var_min,var_max) in variables.items():
                # Avoid zero values
                if var_max == 0:
                    continue
                max_val = ModelScaler.max_val(var_name) 
                
                # Account for the fact that some variables are represented with different bit-ranges by Loihi.
                # Since we're interested in their true value after the alignment, we account for the implied shift
                # here.
                if var_name in ModelScaler.msb_align_act:
                    max_val = max_val * 2**LOIHI2_SPECS.MSB_Alignment_Act
                elif var_name in ModelScaler.msb_align_decay:
                    max_val = max_val * 2**LOIHI2_SPECS.MSB_Alignment_Decay
                elif var_name in ModelScaler.msb_align_prob:
                    max_val = max_val * 2**LOIHI2_SPECS.MSB_Alignment_Prob
                    
                if var_name in ['v','v_th','v_rs']:
                    max_A = (max_val-1)/var_max
                elif var_name in ['j','bias']:
                    max_A = (max_val-1)*alpha_t/var_max
                elif var_name == 'w':
                    max_A = (max_val-1)*alpha_t/var_max
                    
                overall_max_A = min(max_A,overall_max_A)

            assert overall_max_A >= ModelScaler.min_scaling_params(variables)['A'], "Parameter ranges not compatible for F2F conversion."

            return {'alpha_t': alpha_t, 'A': overall_max_A}


