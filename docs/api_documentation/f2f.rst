F2F
===

.. autoclass:: brian2lava.utils.f2f.F2F
   :members:
   :undoc-members:
   :show-inheritance:

ModelWiseF2F
============

.. autoclass:: brian2lava.utils.f2f.ModelWiseF2F
   :members:
   :undoc-members:
   :show-inheritance:
