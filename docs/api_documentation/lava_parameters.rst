LavaParameters
==============

.. automodule:: brian2lava.preset_mode.lava_parameters
   :members:
   :undoc-members:
   :show-inheritance:
