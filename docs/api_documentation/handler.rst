PresetProcessHandler
====================

.. autoclass:: brian2lava.preset_mode.handler.PresetProcessHandler
   :members:
   :undoc-members:
   :show-inheritance:

BrianCondEvaluate
=================

.. autoclass:: brian2lava.preset_mode.handler.BrianCondEvaluate
   :members:
   :undoc-members:
   :show-inheritance:

MicrocodeGenerator
==================

.. autoclass:: brian2lava.preset_mode.handler.MicrocodeGenerator
   :members:
   :undoc-members:
   :show-inheritance:
