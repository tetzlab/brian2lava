========
Monitors
========

.. note::
    If you are experiencing issues with state probes on Loihi 2 hardware, please check if `this <https://github.com/lava-nc/lava/issues/799>`_ has already been fixed in your Lava version. Otherwise, you might simply try to run your simulation again (see `here <https://gitlab.com/brian2lava/brian2lava/-/issues/44>`_).

Note that as opposed to Brian, Brian2Lava exhibits the following deviating behavior:

* when defining a spike monitor, using ``record=False`` does not have an effect (see `this issue <https://gitlab.com/brian2lava/brian2lava/-/issues/37>`_),
* when defining a state monitor, the expression ``record=to_record`` can not be used with ``to_record`` being a list of neuron numbers (instead, it is required to set ``to_record=True``).

