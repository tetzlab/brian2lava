=============
Neuron models
=============

.. note::
	At the moment, due to legal restrictions, models for the ``Loihi2`` backend may just be provided to members of the `Intel Neuromorphic Research Community <https://intel-ncl.atlassian.net/wiki/spaces/INRC/pages/1784807425/Join+the+INRC>`_. Thus, the public version of Brian2Lava currently contains models for the ``CPU`` backend only.

In preset mode, which has to be used for Loihi 2, pre-defined neuron models can be used and customized as detailed in the section on :ref:`preset mode<preset_mode>`.

For using Brian2Lava in :ref:`flexible mode<flexible_mode>`, any particular neuron model `that can be defined in Brian <https://brian2.readthedocs.io/en/stable/user/models.html>`_ can be used. However, note that flexible mode is currently restricted to CPU hardware, as detailed :ref:`here<flexible_mode>`.



.. 
	For Loihi 2 hardware, Brian2Lava can only support a limited number of neuron models. Currently, the following models are available:
	* Leaky Integrate-and-Fire (LIF)

..
	* Adaptive-Voltage Leaky Integrate-and-Fire (ALIF)
	* Adaptive-Threshold Leaky Integrate-and-Fire (AdThLIF)
