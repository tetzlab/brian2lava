================
Simulation modes
================
Brian2Lava features two different simulation modes: preset-model and flexible-model mode, briefly called 'preset' and 'flexible' mode.

For information on how to initialize these, also see :doc:`Importing and device setting</user_guide/import_set_device>`.

.. _preset_mode:

Preset mode
===========
In preset mode, instead of dynamically generating the code to be run in Lava, we use a set of predefined models. While a standard library of models is made available with Brian2Lava, models can be
also be added by users themselves at relative ease. For more information on the necessary steps for this, see :doc:`here</user_guide/preset_model_library>`. 
Note that we currently do not include all of the models provided `with Lava <https://github.com/lava-nc/lava/tree/main/src/lava/proc>`_ (setting them up in Brian2Lava requires some additional steps), 
however, support shall grow with the next releases.

The reason why we need preset mode is that in order to run code on the Loihi 2 chip, instructions need to be given in a low-level microcode language.
As of now, there is no compiler that can generically translate code from high-level languages like Python to Loihi 2 microcode. As a consequence, models to run on-chip need to be hand-crafted, 
directly employing low-level microcode. Since this is a time-consuming process, we aim to facilitate the customization of neuron models for Loihi 2 via Brian2Lava's preset mode.

For the given reasons, the functionality of the preset mode is necessarily restricted, most critically with respect to defining arbitrary ODEs for ``NeuronGroup`` and ``Synapses`` objects.
In fact, preset models come with predefined equations, the structure of which cannot be modified. However, the equations can be viewed by the user through a TeX interface, and setting parameter values
can be done in the usual, intuitive Brian 2 manner. In the next section, we give a step-by-step guide on how to use the preset mode.

Using preset mode
-----------------

.. note::
    The version of Brian2Lava's model library that is currently available to the general public only offers models which can be run on ``CPU``.
    Due to legal restrictions, access to the extended model library that includes ``Loihi2`` models can first only be granted to 'Engaged Members' of the
    `INRC <https://intel-ncl.atlassian.net/wiki/spaces/INRC/pages/1784807425/Join+the+INRC>`_.
    If you already are, please get in touch with us and we will provide you with the remaining components needed to run the simulations directly on chip.
    
When setting the device to preset mode, Brian2Lava will automatically scan for preset models available in
its internal library. Optionally, you can provide a path to your custom models by using the ``models_dir``
argument. For a more thorough explanation on how to structure the folders containing your models, see the page about
the :doc:`Preset model library</user_guide/preset_model_library>`.

This should look something like this, including the output from Brian2Lava: ::

    set_device('lava', mode = 'preset', models_dir = '/path/to/models_dir')

    output: 'Loaded models can now be imported directly by their name from 'brian2lava.preset_mode.model_loader'. NOTE: for a list of model names, try running: ModelLoader.available_preset_models()'

As suggested by the output, after running this command, the models will be available to be imported from the ``model_loader`` module. 

**Displaying available models**

To let you know which models are available for this mode, by default a list of the models will be printed when :doc:`setting the device</user_guide/import_set_device>`, 
You may also obtain the list manually by calling the static method ``ModelLoader.available_preset_models()``. The list includes including the information whether the 
models are ``Loihi2-ready`` or ``CPU-only``, and can look something like this: ::

    atrlif -- brian2lava/preset_mode/lib/model_lib/atrlif -- CPU-only
    lif -- brian2lava/preset_mode/lib/model_lib/lif' -- CPU-only
    lif_delta_v_input -- brian2lava/preset_mode/lib/model_lib/lif_delta_v_input -- CPU-only
    lif_delta_v_input_v_rev -- brian2lava/preset_mode/lib/model_lib/lif_delta_v_input_v_rev -- CPU-only
    lif_rp_delta_v_input -- brian2lava/preset_mode/lib/model_lib/lif_rp_delta_v_input -- CPU-only
    lif_v_input_v_rev -- brian2lava/preset_mode/lib/model_lib/lif_v_input_v_rev -- CPU-only
    probspiker -- brian2lava/preset_mode/lib/model_lib/probspiker -- CPU-only

As mentioned before, the base version of the Brian2Lava library will only contain CPU-only models until Intel's copyright restrictions are lifted.

**Displaying model descriptions**

Once imported in your current session, preset models in Brian2Lava come with a two useful features: ``<model_name>.equations`` and ``<model_name>.show()``.

- The ``equations`` attribute is guaranteed to be present in any supported model, and will contain the model equations in a Brian2-compatible format. These can be fed directly into ``NeuronGroup`` as 
  their model definition. For example for a basic ``lif`` model: :: 

    NeuronGroup(1, model = lif.equations)

.. highlight:: none
    
- The ``show()`` method contains some useful information which makes it easier to define your parameters and their units. The output from this method might look something like this: ::

    Model equations (rendered LaTeX)                                                
    --------------------------------------------------------------------------------
    *<equations will be printed out with mathematical typesetting>*


    Brian 2 equations                                                               
    --------------------------------------------------------------------------------
    dv/dt = (v_rev - v + bias)/tau_v : volt                                     
    bias : volt                                                                     
    tau_v_ind : second                                                              


    Brian 2 conditions                                                              
    --------------------------------------------------------------------------------
    th : v > v_th                                                                   
    rs : v = v_rs                                                                   


    Refractory period                                                               
    --------------------------------------------------------------------------------
    <not supported>


    Variables                                                                       
    --------------------------------------------------------------------------------
    +-----------+-----------------------+--------+
    | name      | description           | unit   |
    +===========+=======================+========+
    | v         | voltage               | V      |
    +-----------+-----------------------+--------+
    | bias      | voltage bias input    | V      |
    +-----------+-----------------------+--------+


    Parameters
    --------------------------------------------------------------------------------
    +--------+-----------------------+--------+
    | name   | description           | unit   |
    +========+=======================+========+
    | tau_v  | voltage time constant | s      |
    +--------+-----------------------+--------+
    | v_th   | threshold voltage     | V      |
    +--------+-----------------------+--------+
    | v_rs   | reset voltage         | V      |
    +--------+-----------------------+--------+
    | v_rev  | reversal voltage      | V      |
    +--------+-----------------------+--------+
    | w      | synaptic weight       | V      |
    +--------+-----------------------+--------+

.. note::
    Note that not all settings provided by this method are strictly necessary for the correct functioning of the model (in particular, simple
    declarations of variables or conditions). 
    However, for all *equations*, using the exact provided formulation *is* necessary.

**Pre-defined input arrays**

Brian 2 features so-called ``TimedArray`` objects that serve to provide pre-defined input functions. Brian2Lava currently supports these partially. To do this, the data of
a discretized version of the input function is written to a file and then subsequently read out by a suitable preset model (e.g., the model ``lif_predef_stim_versatile``).

To define a ``TimedArray`` in your Brian script, the following, particular expresion
may be used (e.g., to define the ``bias`` variable as in the example above): :: 

    bias = R*(I_stim_pA(t, i)*pA + I_bg_mean)

In a future release, it shall become possible to define the names of these variable in the preset neuron model configuration.

For some more examples on the usage of the preset mode, see our :doc:`examples page</introduction/examples>`.

.. _flexible_mode:

Flexible mode
=============
Historically, we started developing Brian2Lava with this mode in mind, and it was the only mode available up to Brian2Lava version 1.0.0-alpha.3.
Similar to established Brian 2 devices (e.g., ``cpp_standalone``), it exploits the powerful code generation features of Brian 2, enabling to use 
arbitrary ODEs for defining neuron and synapse models as well as learning rules. Thereby, the user has a great freedom to define complex 
simulation settings. 

However, as pointed out above, the flexible generation mode is currently not applicable to Loihi 2. For this reason, it shall serve
at the moment primarily the general purpose of enabling to set up purely Python-based simulations in Lava, and may in the future be extended
to encompass different hardware platforms.

As a consequence, flexible mode will only see small updates over the next releases. 
Nonetheless, please feel free to play around with it and let us know if you encounter any issues or have ideas for developing it further.


.. Preset mode supported features

.. Flexible mode supported features
