==============
Synapse models
==============

Note that in principle, an arbitrary variety of synaptic models can be implemented in Lava by creating dedicated neuron group objects that are used in
an off-label manner. We can thus implement synaptic function by intercepting the spikes, computing the quantities required by the synaptic model,
and sending out the relevant quantities as input to the postsynaptic neurons. This approach is taken by the 'flexible' mode.

However, implementing this procedure for Loihi 2 hardware is much more complicated due to the need for dynamical generation of low-level microcode 
(also see :doc:`here </user_guide/modes>`). While creating an according compiler is currently outside of the scope of our project,
it may eventually be pursued by the developers of Lava or a community-driven effort. To enable the use of synapses on Loihi 2, currently, Brian2Lava's
'preset' mode takes an approach that employs existing Lava processes and is thus limited to their functionality (see, for example, 
`here <https://github.com/lava-nc/lava/blob/main/tutorials/in_depth/tutorial09_custom_learning_rules.ipynb>`_).


Synapses in preset mode
=======================

.. note::
	At the moment, due to legal restrictions, models for the ``Loihi2`` backend may just be provided to members of the `Intel Neuromorphic Research Community <https://intel-ncl.atlassian.net/wiki/spaces/INRC/pages/1784807425/Join+the+INRC>`_. Thus, the public version of Brian2Lava currently contains models for the ``CPU`` backend only.

On Loihi, synapses are implemented by on-chip hardwired components. This means that implementing arbitrarily complex synaptic models is under many 
circumstances difficult or impossible. 
For this reason, when using the ``Loihi2`` backend (or other backends in the :ref:`preset mode<preset_mode>`), only a fraction of the features of 
``Synapses`` objects in Brian can be provided by Brian2Lava. 

The current limitations on defining ``Synapses`` objects are the following:

- *Naming*: synaptic weights should be named ``w``. This requirement comes from the need to unequivocally determine which
  synaptic variable constitutes the weight. By ``w`` we have chosen a very common naming convention.
- *Model*: currently, learning is not supported, and for that reason the ``model`` argument when defining
  ``Synapses`` is being ignored. In order to avoid potential errors, we suggest to not use this field unless it's for defining variables, like ``model = "w : volt"``.
  Support for learning in preset mode in planned to come with the next release.
- ``on_pre``: due to the limitations mentioned above, we currently only support one type of synaptic transmission, which
  can be summarized by the typical expression ``on_pre = "v += w"``. Using a different expression will typically result
  in a warning and be ignored.
- ``on_post``: since this condition is usually related to learning rules or learning-related variables, it has not been implemented
  at the current stage of development. Any value given to this argument will be ignored.

Thereby, a typical synapse definition could look like this::

	source = NeuronGroup(...)
	target = NeuronGroup(...)
	s = Synapses(source, target, model = 'w : volt', on_pre = 'v += w') # 'v_post += w' is also accepted

Synaptic connections
--------------------
Unlike the initialization of a ``Synapses`` object, synaptic connections can be generated in the same way as in Brian
without any limitations. For more information on how to connect synapses on Brian,
see the `Brian 2 documentation <https://brian2.readthedocs.io/en/stable/user/synapses.html?highlight=synapses#creating-synapses>`_. 


Synapses in flexible mode
=========================
When using Brian2Lava in the :ref:`flexible mode<flexible_mode>`, in principal, any particular synapse model that 
can be defined `in Brian <https://brian2.readthedocs.io/en/stable/user/synapses.html>`_ can be used. However, flexible mode only supports CPU backends, 
and no support for Loihi is to be expected soon.
Furthermore, there are still some restrictions regarding features that have not yet been implemented -- see :doc:`here </dev_guide/roadmap>`.
