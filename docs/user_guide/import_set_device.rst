============================
Importing and device setting
============================

.. note::
	At the moment, due to legal restrictions, models for the ``Loihi2`` backend may just be provided to members of the `Intel Neuromorphic Research Community <https://intel-ncl.atlassian.net/wiki/spaces/INRC/pages/1784807425/Join+the+INRC>`_. Thus, the public version of Brian2Lava currently contains models for the ``CPU`` backend only.

Using Brian2Lava within Brian 2 only requires two steps.

First, import the package::

	import brian2lava

Second, set the ``lava`` device with the desired hardware backend and :doc:`mode</user_guide/modes>`::

	set_device('lava', hardware='CPU', mode='preset')

In principle, this can already run your Brian simulation on the Lava engine. However, you may have to use a few additional settings to further 
specify how the code for the simulation is generated and executed. These settings can be modified directly in the ``set_device()`` function.

Settings
========
The full argspec of the ``set_device()`` function in Brian2Lava is given by (including the default values)::

	set_device('lava', 
	           hardware = 'CPU', 
	           mode = 'preset', 
	           models_dir = None, 
	           lava_proc_dir = None, 
	           print_models = False, 
	           num_repr = 'fixed', 
	           use_f2f = False, 
	           binary_accuracy = 0, 
	           decimal_accuracy = 0,
	           use_exp_array = False,
	           variable_updating_disabled = False)

In the following, you will find a more comprehensive description for each argument:

- ``hardware``: At the moment two hardware choices are supported, given as string arguments: ``CPU`` and ``Loihi2``. This allows to seamlessly switch between these two hardware configurations.
  As a rule of thumb, all the features present on ``Loihi2`` will be available on ``CPU``, which can be useful for prototyping your models first before deploying them on Loihi.
  Note that the other way around is not always true: see the information about the ``mode`` setting.
- ``mode``: Brian2Lava comes with two simulation modes, again given as string 
  arguments:

			- ``flexible`` mode is currently only supported for the ``CPU`` backend. In this mode we exploit the powerful code generation features of Brian 2, enabling to use arbitrary ODEs for defining 
			  neuron and synapse models as well as learning rules. A large part of the default Brian features are fully supported in this mode. Ideally, in the future this mode
			  will be supported on Loihi 2 as well, but for technical reasons this is not feasible yet. For more information, see :doc:`Modes </user_guide/modes>`.
			- ``preset`` mode uses a library of preset models to generate the Lava code required to run the simulation. Thus, the level of code customization is somewhat limited
			  and the amount of supported features is reduced. Hoewever, this is currently the only mode which supports the ``Loihi2`` hardware. 
- ``models_dir``: An optional argument only used in ``preset`` mode. It specifies the path to an external directory containing additional models to be used in this mode. Useful for user-made models or models which are 
  not present in the default preset library.
- ``lava_proc_dir``: An optional argument only used in ``preset`` mode. It specifies the path to an external directory containing adapted Lava processes to be used in this mode. This is only needed for certain processes
  to be used with Lava-Loihi (i.e., only for Loihi 2 hardware).
- ``print_models``: An optional argument only useful for ``preset`` mode. It specifies if a list of the available models should be printed. This can also be done manually at any time by calling 
  ``ModelLoader.available_preset_models()``.
- ``num_repr``: The number representation to be used. This setting is only relevant for ``preset`` mode. The available settings are ``float`` or ``fixed``. The ``float`` option is only supported
  on ``CPU`` hardware (therefore mainly useful for initial prototyping). Due to the architecture of the chip, the only number representation allowed on ``Loihi2`` is fixed-point representation.
  When using ``fixed`` point-representation, the numbers used in the simulation must all be integers. Nonetheless, we implemented a :doc:`Float2Fixed converter</user_guide/float2fixed>` to facilitate this task. Using this, you won't 
  need to convert all your parameters to integer values, and can seamlessly switch between number representations. Note that this type of conversion may introduce flaws,  
  and some tweaking of your parameters might be required.
- ``use_f2f``: Only relevant for ``preset`` mode with ``fixed`` representation. If you want to use our :doc:`Float2Fixed converter</user_guide/float2fixed>`, set this flag to ``True``. Otherwise, the parameters will be assumed to be in fixed-point 
  representation already, and only rounding will be done. Furthermore, in the default ``False`` case, use of any floating-point number will throw a warning (which also holds for the timestep ``dt``).
- ``binary_accuracy``: Only relevant for ``preset`` mode with ``fixed`` representation. Allows to specify an integer representing the desired additional accuracy for the float-to-fixed point translation. 
  For more info on this argument, see the page on :doc:`Float2Fixed conversion </user_guide/float2fixed>`.
- ``decimal_accuracy``: Only relevant for ``preset`` mode with ``fixed`` representation. Allows to specify an integer representing the desired additional accuracy for the float-to-fixed point translation. 
  Can only be used if ``binary_accuracy`` is not specified, in which case binary accuracy is estimated from it. For more info on this argument, see the page on :doc:`Float2Fixed conversion </user_guide/float2fixed>`.
- ``use_exp_array``: For synaptic weights in fixed-point number representation, Lava uses a mantissa-and-exponent representation. Beyond Lava's current capabilities, Brian2Lava supports the use of individual 
  exponents for each weight value, which is switched on by setting ``use_exp_array = True``. Note that this requires the use of customized implementations of synapse processes for CPU (included in the Brian2Lava package) and Loihi 2. 
- ``variable_updating_disabled``: Offers the opportunity to switch off the updating of Brian class attributes after running the simulation. This can help to save resources and to avoid issues inherent to Lava or Loihi.


Reinstation
===========

If you want to reinstate the device, for example, for multiple ``run()`` calls, keep in mind that the build options that have been passed to ``set_device()`` will be lost in the case of simply calling ``activate()``.
This may be prevented by calling the method as follows::

	device = get_device()
	device.reinit()
	device.activate(**device.build_options)
	