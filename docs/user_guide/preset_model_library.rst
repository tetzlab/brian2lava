=====================
Preset model library
=====================

.. note::
	At the moment, due to legal restrictions, models for the ``Loihi2`` backend may just be provided to members of the `Intel Neuromorphic Research Community <https://intel-ncl.atlassian.net/wiki/spaces/INRC/pages/1784807425/Join+the+INRC>`_. Thus, the public version of Brian2Lava currently contains models for the ``CPU`` backend only.

.. note::
    If you want to contribute to the project, or are already working on implementing a custom neuron model for
    Loihi 2, please `get in touch <https://brian2lava.gitlab.io/>`_ -- any new model added to our library will make a difference! 

To run custom neuron models on Loihi 2, as of the end of 2023, the Neurocore microcode needs to be provided explicitly in a file. 
To cope with this, Brian2Lava comes with a library of preset models whose parameter values can be adjusted by the user. 
We strive to extend this library -- and to this end, we provide here a guide to make a preset neuron model ready
to be used with Brian2Lava. Mainly, this will boil down to a set of standardized rules for file organization and naming.

Folder structure
----------------

In order to set up a model to be run with Brian2Lava, a simple, yet specific folder organization is required. Files should be separated by model into different folders.
For example, if two models ``model_A`` and ``model_B`` are present, the folder organization should look something like this: ::
    
    models/
        |--model_A/
        └--model_B/

The ``models`` folder is not strictly required, Brian2Lava will automatically locate compatible files in any folder given through the ``models_dir`` argument in ``set_device()``. 
See :doc:`here </user_guide/import_set_device>` for more information on all the arguments that can be used with ``set_device()``.
Nonetheless, we strongly recommend using this folder structure (for more on how to import models, see the page on :ref:`preset mode<preset_mode>`).

Required files
--------------

.. note::
    The ``"*"`` character in the file names below means that what comes before is disregarded. For example, a ``json`` file named ``model.json`` will be correctly detected
    by Brian2Lava, but a file ``process_A.py`` will not, since the required ending of a ``Process`` file is strictly ``process.py``. 

In order to run the models in Brian2Lava, a few files are required:

* ``*process.py`` file containing the definition of the ``AbstractProcess`` for the custom model. Only one such file per model is allowed.
* ``*process_model.py`` file containing the ``ProcessModel`` definition for the model. Any number of ``process_model`` files can be present in the model folder. The correct ``ProcessModel`` will be selected automatically at runtime by reading tag and hardware requirements in the ``ProcessModel`` decorators.
* ``*.json`` file containing some fundamental properties for this model. Only one ``json`` file per model is allowed. More information can be found in the next section.
* ``{loihi_microcode}`` file, which contains the code to be sent to the Neurocores of the Loihi 2 chip for execution. Currently, further information about this code is confidential and may only be shared with members of the INRC (visit `this page <https://intel-ncl.atlassian.net/wiki/spaces/INRC/pages/1784807425/Join+the+INRC>`_ if you are interested in joining the INRC).

JSON file
---------

The ``json`` file is required by Brian2Lava in order to correctly find and run the Lava processes. It contains necessary information about the model and
optionally provides information useful to correctly define the model and its parameters in Brian (e.g. parameter names, parameter units, etc.).
The ``json`` file is structured like a typical Python dictionary. The keys expected in this file are:


* ``description``: a short description of the model. Only serves for better user experience.
* ``process_name``: name of the ``Process`` class defining the model contained in the ``*process.py`` file.
* ``ode``: list of string ODEs in `Brian2-compatible string format <https://brian2.readthedocs.io/en/stable/user/models.html>`_. These will be converted into an ``Equations`` object and will be used to determine the parameters required by the model.
* ``latex``, optional: model equations in LaTeX format. These can then be printed by Brian2Lava. Only serves for better user experience.
* ``variables``, optional: list of dictionaries containing "name", "description", and "unit" items, which provide information on the model variables. Only serves UX.
* ``parameters``, optional: list of dictionaries containing "name", "description", and "unit" items, which provide information on the parameters of the model. Only serves UX.
* ``ucode_extensions``: Python dictionary containing "file" and "template" items indicating the extensions of the microcode file and microcode template (if applicable).

The example below demonstrates what the ``json`` file may look like.

**Example**
:: 

    {
		"ode": [
			"dj/dt = -j/tau_j : volt/second",
			"dv/dt = -v/tau_v + j + bias : volt (unless refractory)"
		],
		"latex": [
			"\\frac{dj}{dt} = -j(t) \\cdot \\tau_j^{-1}",
			"\\frac{dv}{dt} = -v(t) \\cdot \\tau_v^{-1} + j(t) + b(t)"
		],
		"variables": [
			{ "name": "j", "description": "current (red.)", "unit": "V/s" },
			{ "name": "v", "description": "voltage", "unit": "V/s" }
		],
		"parameters": [
			{ "name": "tau_j", "description": "current time constant", "unit": "s" },
			{ "name": "tau_v", "description": "voltage time constant" , "unit": "s"},
			{ "name": "bias", "description": "bias current (red.)" , "unit": "V/s"},
			{ "name": "v_th", "description": "threshold voltage", "unit": "V" },
			{ "name": "v_rs", "description": "reset voltage", "unit": "V" }

		],
		"process_name": "LIF",
		"ucode_extensions": {"file": "xyz", "template": "xyz_"}
	}
	

Neuron model library
--------------------

A comprehensive list of neuron models present in our library will be provided as soon as the first release with Loihi support is out. 

