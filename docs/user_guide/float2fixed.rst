===========================
Float2Fixed (F2F) converter
===========================

*This page provides a high-level explanation of the F2F conversion. If you are interested
in the details, you may want to have a look at* :doc:`this </dev_guide/f2f_in_depth>` *page.*

On Intel's Loihi chips, arithmetic operations can only be performed in 
`fixed-point number representation <https://en.wikipedia.org/wiki/Fixed-point_arithmetic>`_. 
This essentially means that only integer values can be used for on-chip computation (exponents can be specified, 
but the actual conversion from/to fractional values is up to the user). However, many simulations that are run in Brian 
require floating-point operations. Therefore, to run a Brian 2 simulation on a Loihi chip, considerable refactoring of 
all parameter values is needed (which can become complicated especially when considering values with physical units). 

In order to make up for this, Brian2Lava introduces a Float2Fixed (F2F) converter. This -- with some considerable limitations -- 
can automatically convert floating-point parameter values to integer values,
and convert them back to floating-point values after the simulation has been run. 

When you use fixed-point number representation (setting ``num_repr=fixed`` in ``set_device()``, cf. 
:doc:`Importing and device setting </user_guide/import_set_device>`), you can simply set the flag ``use_f2f`` to ``True``, and Brian2Lava 
will use its converter to do the mentioned translation between floating- and fixed-point representation.
Note that due to the intrinsic behavior of the converter, the converter shouldn't have any effect if all of your parameters 
are already given in the form of integers. In this case, the converter may be used as an extra 
assurance that your model will run with the correct parameter values on Loihi and other fixed-point implementations.

.. note ::
    The F2F converter is currently still in a testing stage. Please let us know if you encounter any issues.

Conversion Approach
===================
The F2F converter uses a process-by-process conversion approach, in which each process gets its own optimal scaling parameters computed
on the parameter values defined by the user and the requested model. As a quick representative example, consider a LIF neuron with equations:

.. code-block:: python

    eq ="""
        dj/dt = -j/tau_j : volt/second                                                  
        dv/dt = -v/tau_v + j + bias : volt (unless refractory)                          
        """
    threshold = 'v > v_th'
    reset = 'v = 0'

When converting to fixed point numbers, we want to minimize the accuracy loss incurred by the quantization of the variable values,
so that in principle, we want to find a scaling parameter ``A>>1`` that minimizes the quantization error. So ``A`` should be as 
large as possible. In order to do so, we consider two main factors:

1. Each parameter on Loihi is assigned a strict amount of memory at compile time, and as such it's important that multiplying the simulation
   parameters by ``A`` does not exceed the memory limitations of these parameters.

2. We can find heuristic rules to impose upper limits on the variables of our model (in this case only ``v`` and ``j``) by knowing their behavior. In this case:

   - ``v``: 
     ``v_th`` effectively constitutes a hard upper boundary to the values that this variable can take, so that maximizing the value of ``v_th`` ensures that 
     we will minimize the loss due to quantization, since the range of values available for the ``v`` variable will be as large as possible.

   - ``j``: in this case the limit is not as rigid as for the voltage, so we have to work with a bit of heuristic reasoning. We know that any incoming spike will 
     be added to the current variable in the form of a synaptic activation. One simple rule that ensures a certain stability could be to make sure that this variable
     can accomodate a few incoming spikes without overflowing. So we could set ``j_max = j + k*max(w)`` as our upper boundary. At the moment ``k`` is set by 
     testing the model under various stimulation conditions and finding a reasonable value, but in the future it could also be set by the user to manually tune the F2F behavior if needed.

Once we find the optimal scaling parameter for each variable, we choose the smallest one and use it to scale the other parameters. Using the smallest ensures that we will stay 
within the allowed bit-ranges for all of the variables of the model.

For example, let's say we have ``v_th= 0.5`` and Lava allocates 8 bits for the ``v`` variable. We could choose ``A = 128/0.5 = 256`` (accounting for the sign bit).
Say we had a current ``j = 0.1`` and weight ``w = 0.05``. Assuming ``j`` is also allocated 8 bits, then taking ``k=2`` we'd have ``j_max = 0.2`` which would result in ``A = 128/0.2 = 640``. 
Since ``A`` is shared between ``j`` and ``v`` however, using this value would result in an overflow for the ``v`` variable. Then we must choose the lower value for our scaling constant, 
resulting in ``A = 256``.

Scaling weights
---------------

As anticipated, using this conversion approach, each NeuronGroup will be assigned specific scaling parameters, which raises the question on how to scale the communication
between different NeuronGroups that have different scaling factors. Our approach relies on the fact that communication between neurons is at the moment only available
through binary spikes. This simplifies the problem, allowing us to simply enforce the same scaling factors on synaptic weights and postsynaptic neuron variables.
For example, let's say we have a network defined as:
.. code:: 

    NG_A --> Syn --> NG_B

In our approach, we would use one scaling parameter ``A`` for neurongroup A ``NG_A``, and a different one, let's say ``B`` for both the Synaptic weights of ``Syn`` and 
for the variables and parameters of neurongroup B ``NG_B``. This ensures that the incoming activations into ``NG_B`` are appropriately scaled.


Concluding Remarks
------------------

Some additional considerations:

1. In our implementation, we use two scaling parameters instead of one, due to a discrepancy in the Brian2 integration method and the one used by Lava (a 
   more detailed description will not be given here, but will be available in a different wiki page, or external resource). If this were not needed, then the conversion
   process would be much more straight forward and model-agnostic, since all variables would be scaled by the same factor. You can learn more about this by reading the 
   :doc:`developer guide for the F2F converter </dev_guide/f2f_in_depth>`

2. In the actual implementation of the F2F we check the memory limits of each of the parameters of the model and make sure that after the conversion they do not 
   exceed them.

3. One way of accounting for graded spikes and other non-binary signals could still be accounted for by scaling them by the ratio of the scaling factors of pre- and post-synaptic neurons, 
   but this feature is not supported yet. 

4. Some parameters require some additional preprocessing due to the specific treatment they receive by Lava and Loihi. This preprocessing is not taken care of the F2F converter,
   and is instead being handled by the LavaParameters class.
