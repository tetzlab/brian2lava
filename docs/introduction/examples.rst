========
Examples
========

.. note::
	Brian2Lava is still in its testing phase. Please feel free to `report issues or feature requests <https://gitlab.com/brian2lava/brian2lava/-/issues>`_.

.. note::
	At the moment, due to legal restrictions, models for the ``Loihi2`` backend may just be provided to members of the `Intel Neuromorphic Research Community <https://intel-ncl.atlassian.net/wiki/spaces/INRC/pages/1784807425/Join+the+INRC>`_. Thus, the public version of Brian2Lava currently contains models for the ``CPU`` backend only.

Brian2Lava features a library of examples to showcase its usage. Here, you can have a direct look at the currently available examples:

.. 
    Keep the following *-OF-INSERTION labels in place, and do not use them twice!
    
.. START-OF-INSERTION

.. toctree::
    :maxdepth: 1

    lif_small_network_generic
    exc_inh_network_brunel_2000
    lif_rp_v_input_small_network_generic
    network_cuba_synapses
    small_plastic_network_flexible_mode

.. END-OF-INSERTION

Note that more examples will be added in :doc:`following releases <../dev_guide/roadmap>`.
You can retrieve the latest versions of the examples from the `Brian2Lava repository <https://gitlab.com/brian2lava/brian2lava/-/tree/main/examples>`_, or download all of them
at once in a `zip archive <https://gitlab.com/brian2lava/brian2lava/-/archive/main/brian2lava-main.zip?path=examples>`_. 
