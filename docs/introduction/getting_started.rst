===============
Getting started
===============

.. note::
	Brian2Lava is still in its testing phase. Please feel free to `report issues or feature requests <https://gitlab.com/brian2lava/brian2lava/-/issues>`_.

.. note::
	At the moment, due to legal restrictions, models for the ``Loihi2`` backend may just be provided to members of the `Intel Neuromorphic Research Community <https://intel-ncl.atlassian.net/wiki/spaces/INRC/pages/1784807425/Join+the+INRC>`_. Thus, the public version of Brian2Lava currently contains models for the ``CPU`` backend only.

Installation
============

Brian2Lava can be easily installed via the Python Package Index (``pip``)::

	python3 -m pip install brian2lava

After installing the code, you can ensure that the installation was successfull by running the test suite::

	python3 -m brian2lava.tests.run_tests

Running the tests should not take more than 1-2 minutes. After this you should see the results of the tests.
If any of the tests fails, try reinstalling the package (preferrably into an isolated environment to avoid potential
conflicts). If this doesn't solve the issue, feel free to get in touch with us and we'll try to troubleshoot the problem.

Note: ``conda`` support may be added at a later point.

For the latest source code, please visit `gitlab.com/brian2lava/brian2lava <https:/gitlab.com/brian2lava/brian2lava/>`_.
If you run from source code, make sure that you have added your path to the source code to ``PYTHONPATH``, for example, via::

	export PYTHONPATH=~/brian2lava


Import package and set device
=============================

For information on how to import Brian2Lava and set up the related device in Brian 2, please see :doc:`here </user_guide/import_set_device>`.


Example simulations
===================

You may want to continue by considering the example code provided :doc:`here </introduction/examples>`.