================
About Brian2Lava
================

`Brian <https://briansimulator.org/>`_ is an open-source Python package developed and used by the computational neuroscience community to simulate spiking neural networks. The goal of the **Brian2Lava** open-source project is to develop a Brian device interface for the neuromorphic computing framework `Lava <https://lava-nc.org/>`_, in order to facilitate deployment of brain-inspired algorithms on Lava-supported hardware and emulator backends. 
The main purpose of Brian2Lava is to accelerate neuromorphic model prototyping by

* harnessing the power of **differential-equation-based model definitions**, 
* enabling the **execution of existing Brian models**, and 
* facilitating **deployment on different backend systems** via Lava.

The features of Brian2Lava can, moreover, be beneficial for any stage of model simulation beyond prototyping. Thus, the project aims to **reduce entry barriers** and **enable more efficient algorithm development** for using neuromorphic hardware such as Loihi 2.
