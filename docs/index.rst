========================
Brian2Lava documentation
========================

..
   Introduction
   ------------

.. toctree::
   :caption: Introduction
   :maxdepth: 1

   introduction/about
   introduction/getting_started
   introduction/examples

Besides a brief introduction of Brian2Lava, this section collects advice about :doc:`how to get started<introduction/getting_started>` and provides :doc:`examples<introduction/examples>` for using Brian2Lava. This information is also provided on `our website <https://brian2lava.github.io/>`_.


..
   User's guide
   ------------

.. toctree::
   :caption: User's guide
   :maxdepth: 1

   user_guide/import_set_device
   user_guide/monitors
   user_guide/modes
   user_guide/neuron_models
   user_guide/synapse_models
   user_guide/preset_model_library
   user_guide/float2fixed


Most things that a user has to know to use Brian2Lava are described in the `documentation of Brian 2 <https://brian2.readthedocs.io/en/stable/index.html>`_.
Complementary to this, important Brian2Lava-specific aspects are described here.

.. 
	"on the following pages"

..
   Developer's guide
   -----------------

.. toctree::
   :caption: Developer's guide
   :maxdepth: 1

   dev_guide/contributing
   dev_guide/roadmap
   dev_guide/building_and_versioning
   dev_guide/f2f_in_depth
   dev_guide/important_notes_on_brian
   dev_guide/important_notes_on_lava

You want to improve Brian2Lava? Here you'll find information that is important for making a contribution.

Also note that we extend this documentation gradually, so please, feel free to contribute to the documentation as well!

.. 
   API documentation
   -----------------

.. toctree::
   :caption: API documentation
   :maxdepth: 1

   api_documentation/brian2lava.device
   api_documentation/codeobject
   api_documentation/lava_generator
   api_documentation/templater
   api_documentation/abstract_model
   api_documentation/handler
   api_documentation/lava_parameters
   api_documentation/model_loader
   api_documentation/f2f


This part of the documentation provides a reference to the classes comprised by Brian2Lava.


Indices
-------

* :ref:`genindex`
* :ref:`Python Module Index <modindex>`

