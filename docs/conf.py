# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join('..', 'brian2lava', 'codegen')))
sys.path.insert(0, os.path.abspath(os.path.join('..', 'brian2lava', 'device')))
sys.path.insert(0, os.path.abspath(os.path.join('..', 'brian2lava', 'preset_mode')))
sys.path.insert(0, os.path.abspath(os.path.join('..', 'brian2lava', 'utils')))
sys.path.insert(0, os.path.abspath(os.path.join('..', 'brian2lava', 'writer')))
sys.path.insert(0, os.path.abspath(os.path.join('..', 'brian2lava')))
sys.path.insert(0, os.path.abspath('..'))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Brian2Lava'
copyright = '2025'
version = 'v1.0.0-beta.2'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration
master_doc = 'index'

#default_role = 'code' # to use single backtics for code (as in markdown)

extensions = ['sphinx.ext.napoleon', 'sphinx.ext.autodoc', 'nbsphinx']

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
# html_static_path = ['_static'] # currently not used

# -- Adds "Edit on GitLab" to the right top corner ---------------------------
# Source: https://docs.readthedocs.io/en/stable/guides/edit-source-links-sphinx.html
html_context = {
    "display_gitlab": True,
    "gitlab_user": "brian2lava",
    "gitlab_repo": "brian2lava",
    "gitlab_version": "main",
    "conf_py_path": "/docs/",
}

# -- Copy example notebooks --------------------------------------------------
source_dir = '../examples/'
dest_dir = 'introduction/'
insertion_content = [".. toctree::\n", "    :maxdepth: 1\n", "\n"]
# Clean up in destination directory
for file_name in os.listdir(dest_dir):
    if file_name.endswith(".ipynb"):
        try:
            os.remove(os.path.join(dest_dir, file_name))
        except Exception as e:
            print(f"Error removing notebook '{file_name}': {e}")
# Iterate over each file in the source directory and copy if it ends with .ipynb
for file_name in os.listdir(source_dir):
    if file_name.endswith(".ipynb") and not "_compare" in file_name:
        source_path = os.path.join(source_dir, file_name)
        dest_path = os.path.join(dest_dir, file_name)
        with open(source_path, "r", encoding="utf-8") as source_file:
            content = source_file.read()
        with open(dest_path, "w", encoding="utf-8") as dest_file:
            dest_file.write(content)
        print(f"Copied example notebook '{file_name}'.")
        insertion_content.append("    " + file_name.replace(".ipynb", "") + "\n")

# -- Add example notebooks to TOC (based on previous 'examples_list_generator')
before_insertion = True
file_start = []
after_insertion = False
file_end = []
# Keep read and write separate to make sure we don't accidentally delete the whole file
with open(os.path.join(dest_dir, 'examples.rst'), 'r') as file:
    for line in file:
        # Ordering matters, we want to keep the indicators in the file!
        if before_insertion:
            file_start.append(line)
        if 'START-OF-INSERTION' in line:
            before_insertion = False
            file_start.append("\n")
        if 'END-OF-INSERTION' in line:
            after_insertion = True
            file_end.append("\n")
        if after_insertion:
            file_end.append(line)

if before_insertion:
    raise IndexError("Could not locate the entry point for inserting the list."
                     " Make sure that a comment indicating START-OF-INSERTION is present in the examples file.")
if not after_insertion:
    raise IndexError("Could not locate the exit point for the list." 
                     " Make sure that a comment indicating END-OF-INSERTION is present in the examples file.")

with open(os.path.join(dest_dir, 'examples.rst'), 'w') as file:
    complete_file = file_start + insertion_content + file_end
    file.writelines(complete_file)