# (Re-)initialize repository
git init

# make sure the submodules are initialized
git submodule init

# update
git submodule update

# Get latest submodule version
git submodule foreach 'git pull origin main'
