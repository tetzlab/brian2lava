# Clear dist/ directory
#rm -R dist/*

# Install requirements
python3 -m pip install setuptools wheel twine

# Build source distribution
python3 setup.py submodule_update test sdist

# Build wheel
python3 setup.py bdist_wheel

# Test install the wheel (replace 'xyz' by the actual description of the latest wheel)
#python3 -m pip install --upgrade dist/brian2lava-xyz.whl

# Upload release to 'test' pip
#twine upload --repository-url https://test.pypi.org/legacy/ dist/*

# Upload release to pip
#twine upload dist/*

# Build the documentation
cd docs/
make clean html
cd ..

# Upload the documentation
# TODO